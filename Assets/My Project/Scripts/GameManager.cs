using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] TextMeshProUGUI recordTXT;
    [SerializeField] TextMeshProUGUI scoreTXT;

    public GameObject panelStart;
    public GameObject panelGameOver;

    float record;
    [SerializeField] float score;

    public bool gameStarted = false;

    private void Awake()
    {
        instance = this;
        record = PlayerPrefs.GetInt("Record", 0);
        recordTXT.text = record.ToString("0000");

        panelStart.SetActive(true);
        panelGameOver.SetActive(false);
    }
    
    public void AddScore()
    {
        score += Time.time;
        if (score > 0)
        {
            scoreTXT.text = score.ToString("0000");
        }
    }

    public void UpdateRecord()
    {
        if (score > record)
        {
            record = score;

            PlayerPrefs.SetInt("Record", (int)record);
            recordTXT.text = record.ToString("0000");
        }
    }
    public void StartGame()
    {
        gameStarted = true;
        Player.Instance.PlayerStart();
        panelStart.SetActive(false);
    }

    public void GameOver()
    {
        UpdateRecord();
        panelGameOver?.SetActive(true);
        Invoke("RestartGame", 2f);
    }

    void RestartGame()
    {
        SceneManager.LoadScene(0);
    }



}
