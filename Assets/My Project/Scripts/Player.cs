using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance;
    private Rigidbody2D rb;

    [SerializeField] private bool canJump;
    [SerializeField] private float jumpForce;
    [SerializeField] private float gravityScale;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] private float groundDistance;
    [SerializeField] private Transform groundCheck;

    public float speed;
    public bool death;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        Instance = this;
    }

    private void Update()
    {
        if (!isGround()) return;

        if (Input.GetMouseButtonDown(0) || Input.GetKey(KeyCode.Space))
        {
            canJump = true;
        }
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void OnCollisionEnter2D(Collision2D _other)
    {
        if (death) return;

        if (_other.collider.CompareTag("Obstacle"))
        {
            Death();
        }

    }

    public void PlayerStart()
    {
        rb.gravityScale = gravityScale;
    }

    private void Move()
    {

       if (canJump) 
        {
            canJump = false;
            Vector2 _velocity = rb.velocity;
            _velocity.y = 0;
            rb.velocity = _velocity;
            rb.AddForce(Vector2.up * jumpForce);
        }
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    bool isGround()
    {
        return Physics2D.OverlapCircle(groundCheck.position, groundDistance, groundLayer);
    }

    void Death()
    {
        death = true;
        GameManager.instance.GameOver();
    }

}
