using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjcets : MonoBehaviour
{
    [SerializeField]
    string tagTarget;

    [SerializeField]
    float count;

    [SerializeField]
    float offset;

    private void OnTriggerEnter2D(Collider2D _other)
    {
        //repetir os canos
        if (_other.CompareTag(tagTarget))
        {
            Vector2 _newops = _other.transform.position;
            _newops.x += count * offset;
            _other.transform.position = _newops;
        }
    }
}
