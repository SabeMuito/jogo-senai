using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField]
    private Transform Player;

    private void Update()
    {
        Follow();
    }

    void Follow()
    {
        Vector3 _newops = transform.position;
        _newops.x = Player.position.x;
        transform.position = _newops;
    }


}
